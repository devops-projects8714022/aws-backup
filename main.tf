resource "aws_backup_vault" "example" {
  provider = aws.management
  name     = var.backup_vault_name
}

resource "aws_backup_plan" "example" {
  provider = aws.management
  name     = var.backup_plan_name

  rule {
    rule_name         = var.backup_rule_name
    target_vault_name = aws_backup_vault.example.name
    schedule          = var.schedule

    lifecycle {
      cold_storage_after = var.cold_storage_after
      delete_after       = var.delete_after
    }
  }
}

resource "aws_backup_plan_share" "example" {
  provider          = aws.management
  backup_plan_id    = aws_backup_plan.example.id
  account_id        = var.member_account_id
  share_with_organization = true
}

data "aws_backup_plan" "shared" {
  provider = aws.member
  name     = var.backup_plan_name
}

resource "aws_backup_selection" "example" {
  provider        = aws.member
  plan_id         = data.aws_backup_plan.shared.id
  selection_name  = "example-backup-selection"
  iam_role_arn    = var.iam_role_arn

  resources = [
    var.resource_arn
  ]
}
