management_profile = "management-account"
member_profile     = "member-account"
member_account_id  = "123456789012"
iam_role_arn       = "arn:aws:iam::123456789012:role/service-role/AWSBackupDefaultServiceRole"
resource_arn       = "arn:aws:ec2:us-east-1:123456789012:volume/vol-0123456789abcdef0"
