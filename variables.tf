variable "region" {
  description = "AWS Region"
  type        = string
  default     = "us-east-1"
}

variable "management_profile" {
  description = "AWS CLI profile for management account"
  type        = string
}

variable "member_profile" {
  description = "AWS CLI profile for member account"
  type        = string
}

variable "member_account_id" {
  description = "AWS account ID for member account"
  type        = string
}

variable "backup_vault_name" {
  description = "Name of the backup vault"
  type        = string
  default     = "example-backup-vault"
}

variable "backup_plan_name" {
  description = "Name of the backup plan"
  type        = string
  default     = "example-backup-plan"
}

variable "backup_rule_name" {
  description = "Name of the backup rule"
  type        = string
  default     = "example-backup-rule"
}

variable "schedule" {
  description = "Backup schedule in cron format"
  type        = string
  default     = "cron(0 12 * * ? *)"
}

variable "cold_storage_after" {
  description = "Days after creation to transition to cold storage"
  type        = number
  default     = 30
}

variable "delete_after" {
  description = "Days after creation to delete the backup"
  type        = number
  default     = 365
}

variable "iam_role_arn" {
  description = "IAM role ARN for AWS Backup"
  type        = string
}

variable "resource_arn" {
  description = "ARN of the resource to be backed up"
  type        = string
}
